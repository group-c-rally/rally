#include <stdio.h>
#include <time.h>

#include "nuklear_flags.h"

#include "nuklear.h"
#include "nuklear_sdl_renderer.h"

#include <SDL2/SDL.h>

#include "image.h"
#include "keyboard.h"
#include "log.h"
#include "text.h"
#include "timing.h"
#include "util.h"

int main(int argc, char **argv) {
  timing_init();

  if (SDL_Init(SDL_INIT_EVERYTHING)) {
    log_fatal("SDL_Init error: %s", SDL_GetError());
  }
  SDL_Window *window = SDL_CreateWindow("gaem", SDL_WINDOWPOS_UNDEFINED,
                                        SDL_WINDOWPOS_UNDEFINED, 800, 800, 0);
  if (!window) {
    log_fatal("SDL_CreateWindow error: %s", SDL_GetError());
  }
  SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, 0);
  if (!renderer) {
    log_fatal("SDL_CreateRenderer error: %s", SDL_GetError());
  }

  fonts_init();
  SDL_Texture *text = render_text_fast(renderer, font_18, white, "hej");
  SDL_Rect text_rect = measure_text_rect(font_18, "hej");

  bool running = true;

  u64 last_frame = timing_nsec();
  f32 delta;

  f32 y = 10.0;
  f32 x = 10.0;

  struct nk_context *ctx = nk_sdl_init(window, renderer);

  struct nk_font_atlas *atlas;
  struct nk_font_config config = nk_font_config(0);
  struct nk_font *font;

  float font_scale = 1;

  /* set up the font atlas and add desired font; note that font sizes are
   * multiplied by font_scale to produce better results at higher DPIs */
  nk_sdl_font_stash_begin(&atlas);
  font = nk_font_atlas_add_default(atlas, 13 * font_scale, &config);
  /*font = nk_font_atlas_add_from_file(atlas,
   * "../../../extra_font/DroidSans.ttf", 14 * font_scale, &config);*/
  /*font = nk_font_atlas_add_from_file(atlas,
   * "../../../extra_font/Roboto-Regular.ttf", 16 * font_scale, &config);*/
  /*font = nk_font_atlas_add_from_file(atlas,
   * "../../../extra_font/kenvector_future_thin.ttf", 13 * font_scale,
   * &config);*/
  /*font = nk_font_atlas_add_from_file(atlas,
   * "../../../extra_font/ProggyClean.ttf", 12 * font_scale, &config);*/
  /*font = nk_font_atlas_add_from_file(atlas,
   * "../../../extra_font/ProggyTiny.ttf", 10 * font_scale, &config);*/
  /*font = nk_font_atlas_add_from_file(atlas,
   * "../../../extra_font/Cousine-Regular.ttf", 13 * font_scale, &config);*/
  nk_sdl_font_stash_end();

  /* this hack makes the font appear to be scaled down to the desired
   * size and is only necessary when font_scale > 1 */
  font->handle.height /= font_scale;
  /*nk_style_load_all_cursors(ctx, atlas->cursors);*/
  nk_style_set_font(ctx, &font->handle);

  image_setup("assets/test6x6.png", renderer);

  while (running) {
    // handle events
    SDL_Event event;
    nk_input_begin(ctx);
    while (SDL_PollEvent(&event)) {
      nk_sdl_handle_event(&event);
      switch (event.type) {
      case SDL_QUIT:
        running = false;
        break;
      case SDL_TEXTINPUT:
      case SDL_TEXTEDITING:
      case SDL_KEYDOWN:
      case SDL_KEYUP:
        // ignored
        break;
      default:
        log_debug("unhandled event: %d", event.type);
      }
    }
    nk_input_end(ctx);

    // pre update

    u64 frame = timing_nsec();
    delta = (f32)(frame - last_frame) / 1000000000;
    last_frame = frame;

    keyboard_pre_update();

    // update

    if (keyboard_state[SDL_SCANCODE_J]) {
      y += 100 * delta;
    }
    if (keyboard_state[SDL_SCANCODE_K]) {
      y -= 100 * delta;
    }
    if (keyboard_state[SDL_SCANCODE_H]) {
      x -= 100 * delta;
    }
    if (keyboard_state[SDL_SCANCODE_L]) {
      x += 100 * delta;
    }

    if (nk_begin(ctx, "Demo", nk_rect(50, 50, 230, 250),
                 NK_WINDOW_BORDER | NK_WINDOW_MOVABLE | NK_WINDOW_SCALABLE |
                     NK_WINDOW_MINIMIZABLE | NK_WINDOW_TITLE)) {
      nk_layout_row_static(ctx, 30, 80, 1);
      if (nk_button_label(ctx, "button"))
        log_info("button");
    }
    nk_end(ctx);

    if (just_released(SDL_SCANCODE_A)) {
      log_info("a");
    }

    // post update

    keyboard_post_update();

    // draw

    SDL_SetRenderDrawColor(renderer, 0, 0, 0, SDL_ALPHA_OPAQUE);
    SDL_RenderClear(renderer);
    SDL_SetRenderDrawColor(renderer, 255, 0, 0, SDL_ALPHA_OPAQUE);
    SDL_Rect rect = {(int)x, (int)y, 100, 100};
    text_rect.x = (int)x;
    text_rect.y = (int)y;

    SDL_RenderFillRect(renderer, &rect);
    image_draw(renderer, &rect);

    SDL_RenderCopy(renderer, text, NULL, &text_rect);

    nk_sdl_render(NK_ANTI_ALIASING_ON);

    SDL_RenderPresent(renderer);
  }

  // TODO: window flashes when destroyed?
  // Not for me (mar)
  nk_sdl_shutdown();
  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);
  SDL_Quit();
}
