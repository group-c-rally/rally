#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include "util.h"

extern TTF_Font *font_18;

void fonts_init(void);

SDL_Texture *render_text_fast(SDL_Renderer *renderer, TTF_Font *font,
                              SDL_Color color, const char *text);
SDL_Texture *render_text_fast_with_newlines(SDL_Renderer *renderer,
                                            TTF_Font *font, SDL_Color color,
                                            const char *text);
SDL_Texture *render_text_fast_wrapped_with_newlines(SDL_Renderer *renderer,
                                                    TTF_Font *font,
                                                    SDL_Color color, u32 wrap,
                                                    const char *text);

SDL_Rect measure_text_rect(TTF_Font *font, const char *text);
