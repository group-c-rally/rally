#include "timing.h"

#include "log.h"

struct timespec start;
bool started = false;

bool timing_started(void) { return started; }

void timing_init(void) {
  if (started)
    log_fatal("tried to initialize timing a second time");
  started = true;
  timespec_get(&start, TIME_UTC);
}

u64 timing_nsec(void) {
  struct timespec t;
  timespec_get(&t, TIME_UTC);
  return (t.tv_nsec - start.tv_nsec) + (t.tv_sec - start.tv_sec) * 1000000000;
}

f32 timing_sec(void) { return (f32)timing_nsec() / 1000000000; }
