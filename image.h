#include <SDL2/SDL.h>
#include <SDL2/SDL_error.h>

#define default_channel 0

void image_setup(char *image_path, SDL_Renderer *renderer);
void image_draw(SDL_Renderer *renderer, SDL_Rect *position);
