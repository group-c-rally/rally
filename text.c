#include "text.h"

#include "SDL_render.h"
#include "SDL_ttf.h"

#include "log.h"

TTF_Font *font_18;

void fonts_init(void) {
  if (TTF_Init()) {
    log_fatal("TTF_Init error: %s", SDL_GetError());
  }
  font_18 = TTF_OpenFont("/usr/share/fonts/noto/NotoSans-Medium.ttf", 18);
  if (!font_18) {
    log_fatal("TTF_Open failed: %s", SDL_GetError());
  }
  TTF_SetFontStyle(font_18, TTF_STYLE_NORMAL);
  TTF_SetFontOutline(font_18, 0); // ?
  TTF_SetFontKerning(font_18, 1); // ?
  TTF_SetFontHinting(font_18, TTF_HINTING_NORMAL);
}

SDL_Texture *render_text_fast(SDL_Renderer *renderer, TTF_Font *font,
                              SDL_Color color, const char *text) {
  SDL_Surface *surface = TTF_RenderUTF8_Solid(font, text, color);
  SDL_Texture *texture = SDL_CreateTextureFromSurface(renderer, surface);
  SDL_FreeSurface(surface);
  return texture;
}

SDL_Texture *render_text_fast_with_newlines(SDL_Renderer *renderer,
                                            TTF_Font *font, SDL_Color color,
                                            const char *text) {
  SDL_Surface *surface = TTF_RenderUTF8_Solid_Wrapped(font, text, color, 0);
  SDL_Texture *texture = SDL_CreateTextureFromSurface(renderer, surface);
  SDL_FreeSurface(surface);
  return texture;
}

SDL_Texture *render_text_fast_wrapped_with_newlines(SDL_Renderer *renderer,
                                                    TTF_Font *font,
                                                    SDL_Color color, u32 wrap,
                                                    const char *text) {
  SDL_Surface *surface = TTF_RenderUTF8_Solid_Wrapped(font, text, color, wrap);
  SDL_Texture *texture = SDL_CreateTextureFromSurface(renderer, surface);
  SDL_FreeSurface(surface);
  return texture;
}

SDL_Rect measure_text_rect(TTF_Font *font, const char *text) {
  SDL_Rect rect = {0};
  if (TTF_SizeUTF8(font, text, &rect.w, &rect.h)) {
    log_error("TTF_SizeUTF8 failed: %s", SDL_GetError());
  }
  return rect;
}
