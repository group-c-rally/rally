#include "keyboard.h"

#include <stddef.h>

#include <SDL2/SDL.h>

int last_numkeys = 0;
uint8_t *last_keyboard_state = NULL;

int numkeys = 0;
const uint8_t *keyboard_state = NULL;

void keyboard_pre_update(void) {
  keyboard_state = SDL_GetKeyboardState(&numkeys);
}

void keyboard_post_update(void) {
  // store previous keyboard state. SDL re-uses the keyboard_state
  // pointer so we copy to the store after using it in the update.
  last_keyboard_state = realloc(last_keyboard_state, sizeof(uint8_t) * numkeys);
  memcpy(last_keyboard_state, keyboard_state, sizeof(uint8_t) * numkeys);
  last_numkeys = numkeys;
}

bool check_keyboard_state(int key) {
  return key < last_numkeys && key < numkeys;
}

bool just_pressed(int key) {
  return check_keyboard_state(key) && keyboard_state[key] == 1 &&
         last_keyboard_state[key] == 0;
}

bool just_released(int key) {
  return check_keyboard_state(key) && keyboard_state[key] == 0 &&
         last_keyboard_state[key] == 1;
}
