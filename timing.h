#include <time.h>

#include "util.h"

void timing_init(void);
bool timing_started(void);
u64 timing_nsec(void);
f32 timing_sec(void);
