#ifndef __UTIL_H
#define __UTIL_H

#include <stdbool.h>
#include <stdint.h>

#include "SDL2/SDL_pixels.h"

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;
typedef float f32;
typedef double f64;

extern SDL_Color white;

#endif // __UTIL_H
