#include "image.h"

#include "SDL_stbimage.h"

#include "util.h"

u8 *image;
SDL_Texture *texture;

void image_setup(char *image_path, SDL_Renderer *renderer) {
  SDL_Surface *surface = STBIMG_Load(image_path);
  texture = SDL_CreateTextureFromSurface(renderer, surface);
  SDL_FreeSurface(surface);
}

void image_draw(SDL_Renderer *renderer, SDL_Rect *position) {
  SDL_RenderCopy(renderer, texture, NULL, position);
}
