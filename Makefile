SRC = $(shell find -name "*.c" -not -path "./vendor/*" -not -path "./inc/*")
OBJ = $(patsubst %.c,%.o,$(SRC))
HEADERS = $(shell find -name "*.h" -not -path "./vendor/*")

LINKFLAGS = -lSDL2 -lSDL2_ttf -lm -ldl
CPPFLAGS = $(shell pkg-config --cflags sdl2) -Iinc
CFLAGS = -std=c17 -O2 -Wall -Wpedantic
CC = clang

.PHONY: clean info run

all: build
build: $(OBJ)
	$(CC) $(CFLAGS) $(CPPFLAGS) $^ -o main $(LINKFLAGS)


info:
	$(info SRC       = $(SRC))
	$(info OBJ       = $(OBJ))
	$(info HEADERS   = $(HEADERS))
	$(info LINKFLAGS = $(LINKFLAGS))
	$(info CFLAGS    = $(CFLAGS))
	$(info CC        = $(CC))

run: build
	./main

# https://www.gnu.org/software/make/manual/html_node/Automatic-Prerequisites.html
include $(SRC:.c=.d)
%.d: %.c
	$(info $@)
	@set -e; rm -f $@; \
		$(CC) -MM $(CPPFLAGS) $< > $@.$$$$; \
		sed 's,\($*\)\.o[ :]*,\1.o $@ : ,g' < $@.$$$$ > $@; \
		rm -f $@.$$$$

%.o: %.c
	$(CC) $(CFLAGS) $(CPPFLAGS) -c $<

clean:
	rm -rf *.o *.d main
