#ifndef __LOG_H
#define __LOG_H

enum level {
  DEBUG,
  INFO,
  WARNING,
  ERROR,
  FATAL,
};

void log_message(enum level level, const char *format, ...);
void log_debug(const char *format, ...);
void log_info(const char *format, ...);
void log_warning(const char *format, ...);
void log_error(const char *format, ...);
_Noreturn void log_fatal(const char *format, ...);

#endif // __LOG_H
