#include "log.h"

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include "timing.h"

void log_init(void) {}

void log_message(enum level level, const char *format, ...) {
  va_list argp;
  va_start(argp, format);

  if (timing_started())
    printf("[%.6f] ", timing_sec());

  switch (level) {
  case DEBUG:
    printf("DEBUG ");
    break;
  case INFO:
    printf("INFO  ");
    break;
  case WARNING:
    printf("WARN  ");
    break;
  case ERROR:
    printf("ERROR ");
    break;
  case FATAL:
    printf("FATAL ");
    break;
  }

  vprintf(format, argp);
  printf("\n");
}

void log_debug(const char *format, ...) {
  va_list argp;
  va_start(argp, format);
  log_message(DEBUG, format, argp);
}

void log_info(const char *format, ...) {
  va_list argp;
  va_start(argp, format);
  log_message(INFO, format, argp);
}

void log_warning(const char *format, ...) {
  va_list argp;
  va_start(argp, format);
  log_message(WARNING, format, argp);
}

void log_error(const char *format, ...) {
  va_list argp;
  va_start(argp, format);
  log_message(ERROR, format, argp);
}

void log_fatal(const char *format, ...) {
  va_list argp;
  va_start(argp, format);
  log_message(FATAL, format, argp);
  exit(EXIT_FAILURE);
}
