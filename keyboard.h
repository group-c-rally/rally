#include "util.h"

extern const uint8_t *keyboard_state;

bool just_pressed(int key);
bool just_released(int key);
void keyboard_pre_update(void);
void keyboard_post_update(void);
